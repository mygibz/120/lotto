﻿using System.ComponentModel;

namespace Lotto
{
    public class Generator : BaseModel
    {


        public int Amount { get => _amount; set { _amount = value; OnPropertyChanged("Amount"); } }
        private int _amount;
        public int MinValue { get => _minValue; set { _minValue = value; OnPropertyChanged("Amount"); } }
        private int _minValue;
        public int MaxValue { get => _maxValue; set { _maxValue = value; OnPropertyChanged("Amount"); } }
        private int _maxValue;

        public Generator(int amount, int maxValue, int minValue)
        {
            this._amount = amount;
            this._maxValue = maxValue;
            this._minValue = minValue;
        }
    }

    public class BaseModel : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}